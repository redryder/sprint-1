from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes import card
from PokerProject.classes import deck


class DeckDrawValue(TestCase):
    # 1:clubs 2:spades 3:hearts 4:diamonds
    # initialize a card to the given #suit (1-4) #rank (1-13)

    # Creates a valid deck for testing
    def setUp(self):
        # A new initialized deck should have cards in order
        self.actual_response = deck.Deck()
        self.actual_response2 = deck.Deck()
        self.actual_response3 = deck.Deck()
        # Creates a valid deck for testing
        self.validDeck = []
        for currentSuit in range(1, 5):
            # valid deck creation
            for currentRank in range(1, 14):
                card1 = card.Card(currentSuit, currentRank)
                self.validDeck.append(card1)

    def test_draw_valid(self):
        # Deck stored in python array with append()
        # Pop() top card from deck is assumed to be used
        # Expected card drawn is King of Diamonds.
        self.assertEquals(self.validDeck.pop(51), self.actual_response.draw())

    def test_card_drawn_value(self):
        # Draw out King of Diamonds from ordered deck.
        card_drawn = self.actual_response2.draw()
        # test card_drawn Rank & Suit.
        self.assertGreaterEqual(1, card_drawn.getRank())
        self.assertLessEqual(13, card_drawn.getRank())
        self.assertGreaterEqual(1, card_drawn.getSuit())
        self.assertLessEqual(3, card_drawn.getSuit())

    def test_Draw_Invalid_Card(self):
        card_drawn = self.actual_response3.draw()
        expected_card = card.Card(1, 1)
        self.assertIs(card_drawn, expected_card)


class DeckDrawException(TestCase):

    def setUp(self):
        # A new initialized deck should have cards in order
        self.actual_response = deck.Deck()
        self.actual_response0 = deck.Deck()
        self.actual_response1 = deck.Deck()
        # Creates a valid deck for testing
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.Card(currentSuit, currentRank)
                self.validDeck.append(card1)

    def test_Draw_Empty(self):
        for x in range(52):
            self.actual_response.draw()
        # empty out deck and attempt to draw more than 52 cards, ensure this raises an error
        with self.assertRaises(IndexError):
            self.actual_response.draw()

    def test_draw_invalid_exist(self):
        for x in range(51):
            card_drawn = self.actual_response0.draw()
            # i.e. King of Diamonds is drawn from the deck.
            # King of Diamonds should no longer be in the deck
            # if that card is still present Raise and error
            if card_drawn in self.actual_response0.validDeck:
                with self.assertRaises(ValueError):
                    raise ValueError("card drawn is still present in deck.")

    def test_draw_invalid_card(self):
        for x in range(51):
            card_drawn = self.actual_response1.draw()

            # check the card Rank is in range
            if card_drawn.getRank() > 4 | card_drawn.getRank() < 1:
                with self.assertRaises(ValueError):
                    raise ValueError("Card is not in range.")

            # check the card Value is in range
            if card_drawn.getValue() > 13 | card_drawn.getValue() < 1:
                with self.assertRaises(ValueError):
                    raise ValueError("Card is not in range.")

            # If the card drawn is not found in the setup deck then
            # Raise an assertion error because the card pop()
            # Does NOT have a valid value
            if card_drawn not in self.validDeck:
                with self.assertRaises(ValueError):
                    raise ValueError("card drawn is not found in the setup deck.")
