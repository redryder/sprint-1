from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes import card


class CardStr(TestCase):
    # Card tests
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)

        # Clubs
        self.aceClubs = card.Card(1, 1)
        self.nineClubs = card.Card(1, 9)
        self.twoClubs = card.Card(1, 2)
        # Spades
        self.threeSpades = card.Card(2, 3)
        self.tenSpades = card.Card(2, 10)
        self.fourSpades = card.Card(2, 4)
        # Hearts
        self.aceHearts = card.Card(3, 1)
        self.nineHearts = card.Card(3, 9)
        self.twoHearts = card.Card(3, 2)
        # Diamonds
        self.threeDiamonds = card.Card(4, 3)
        self.tenDiamonds = card.Card(4, 10)
        self.fourDiamonds = card.Card(4, 4)

    def test_Str(self):
        # intentional failures, until __str__ is implemented correctly in card.py
        # Because __str__ is unimplemented, it will always be blank, rather than the correct String.
        # Clubs
        self.assertEquals(self.aceClubs.__str__(), "AC")
        self.assertEquals(self.twoClubs.__str__(), "2C")
        self.assertEquals(self.nineClubs.__str__(), "9C")
        # Spades
        self.assertEquals(self.threeSpades.__str__(), "3S")
        self.assertEquals(self.tenSpades.__str__(), "10S")
        self.assertEquals(self.fourSpades.__str__(), "4S")
        # Hearts
        self.assertEquals(self.aceHearts.__str__(), "AH")
        self.assertEquals(self.nineHearts.__str__(), "9H")
        self.assertEquals(self.twohearts.__str__(), "2H")
        # Diamonds
        self.assertEquals(self.threeDiamonds.__str__(), "3D")
        self.assertEquals(self.tenDiamonds.__str__(), "10D")
        self.assertEquals(self.fourDiamonds.__str__(), "4D")


class CardStrRaiseValueError(TestCase):
    def test_RaiseValueError(self):
        # 0 is the default value # will change once Poker Card Values Set
        self.assertEqual(0, self.aceClubs.getValue())
        self.assertEqual(0, self.twoClubs.getValue())
        self.assertEqual(0, self.threeSpades.getValue())

        # Clubs
        self.aceClubs.value = "AC"
        # Spades
        self.threeSpades.value = .000001
        # Hearts
        self.aceHearts.value = "ausgfsjhdagf"
        # Diamonds
        self.threeDiamonds.value = -1

        with self.assertRaises(ValueError):
            self.aceClubs.getValue()
        with self.assertRaises(ValueError):
            self.threeSpades.getValue()
        with self.assertRaises(ValueError):
            self.aceHearts.getValue()
        with self.assertRaises(ValueError):
            self.threeDiamonds.getValue()
