import unittest
from _UnitTests.test_CardGet import *
from _UnitTests.test_CardInit import *
from _UnitTests.test_CardStr import *
from _UnitTests.test_DeckInit import *
from _UnitTests.test_DeckDraw import *

# ****************************************************
# Right Click and run this file to run UnitTests.
# this is a file using unittest to run tests locally
# ****************************************************
# Sprint2 Additions: HandTest, PokerAdminTest, PlayerTest
from _UnitTests.test_Hand import *
from _UnitTests.test_Player import *
from _UnitTests.test_Dealer import *


def suite():
    # Add all the classes, each class will be a different "test Suite" just a group of tests to see if everything in the class is functioning correctly
    suites = [CardInitCorrectRankAndSuit, CardInitRaiseValueError, CardGetSuite, CardGetRank, CardGetValue, CardStr,
              DeckInit, DeckCount, DeckDrawValue, DeckDrawException, DeckShuffle]
    suite = unittest.TestSuite()
    for s in suites:
        suite.addTest(unittest.makeSuite(s))
        # this code will just loop through an array with all the class names and add them to the suite object.
    return suite

#code to run the tests; PyCharm does this automatically
#so this code could be removed in PyCharm
suite = suite()
runner = unittest.TextTestRunner()
res=runner.run(suite)
print(res)
print("*"*20)
for i in res.failures: print(i[1])