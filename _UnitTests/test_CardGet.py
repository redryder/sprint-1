from django.test import TestCase
# from PokerProjectDjango.PokerProject.classes import deck
# from unittest import TestCase
from PokerProject.classes import card


class CardGetSuite(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.Card(1, 1)
        self.c2 = card.Card(1, 2)
        self.c3 = card.Card(2, 4)
        self.c4 = card.Card(2, 5)
        self.c5 = card.Card(3, 7)
        self.c6 = card.Card(3, 8)
        self.c7 = card.Card(4, 10)
        self.c8 = card.Card(4, 11)

    # This test will be to test that the getSuit() method of a card will return an accurate value
    def test_GetSuitAccuracy(self):
        # check that, if the card's suit is x, its getSuit() method returns x
        self.assertEqual(self.c1.getSuit(), 1)
        self.assertEqual(self.c2.getSuit(), 1)
        self.assertEqual(self.c3.getSuit(), 2)
        self.assertEqual(self.c4.getSuit(), 2)
        self.assertEqual(self.c5.getSuit(), 3)
        self.assertEqual(self.c6.getSuit(), 3)
        self.assertEqual(self.c7.getSuit(), 4)
        self.assertEqual(self.c8.getSuit(), 4)

    # This test is to ensure that no suit is outside of the acceptable range
    def test_GetSuitRange(self):
        # test(s)
        self.assertLessEqual(self.c1.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c1.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c3.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c3.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c5.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c5.getSuit(), 1)  # acceptable range of suits (1-4).
        self.assertLessEqual(self.c7.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(self.c7.getSuit(), 1)  # acceptable range of suits (1-4).

class CardGetRank(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.Card(1, 1)
        self.c2 = card.Card(1, 2)
        self.c3 = card.Card(2, 4)
        self.c4 = card.Card(2, 5)
        self.c5 = card.Card(3, 7)
        self.c6 = card.Card(3, 8)
        self.c7 = card.Card(4, 10)
        self.c8 = card.Card(4, 11)

    # This test will be to test that the getRank() method of a card will return an accurate value
    def test_GetRankAccuracy(self):
        # test(s)
        self.assertEqual(self.c1.getRank(), 1)
        self.assertEqual(self.c2.getRank(), 2)
        self.assertEqual(self.c3.getRank(), 4)
        self.assertEqual(self.c4.getRank(), 5)
        self.assertEqual(self.c5.getRank(), 7)
        self.assertEqual(self.c6.getRank(), 8)
        self.assertEqual(self.c7.getRank(), 10)
        self.assertEqual(self.c8.getRank(), 11)

    def test_GetRankRange(self):
        # This will be used to make sure any card drawn is within the
        # acceptable range of ranks (1-13).
        self.assertLessEqual(self.c1.getRank(), 13)
        self.assertGreaterEqual(self.c1.getRank(), 1)
        self.assertLessEqual(self.c2.getRank(), 13)
        self.assertGreaterEqual(self.c2.getRank(), 1)
        self.assertLessEqual(self.c3.getRank(), 13)
        self.assertGreaterEqual(self.c3.getRank(), 1)
        self.assertLessEqual(self.c4.getRank(), 13)
        self.assertGreaterEqual(self.c4.getRank(), 1)
        self.assertLessEqual(self.c5.getRank(), 13)
        self.assertGreaterEqual(self.c5.getRank(), 1)
        self.assertLessEqual(self.c6.getRank(), 13)
        self.assertGreaterEqual(self.c6.getRank(), 1)
        self.assertLessEqual(self.c7.getRank(), 13)
        self.assertGreaterEqual(self.c7.getRank(), 1)
        self.assertLessEqual(self.c8.getRank(), 13)
        self.assertGreaterEqual(self.c8.getRank(), 1)

class CardGetValue(TestCase):
    def setUp(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.c1 = card.Card(1, 1)
        self.c2 = card.Card(1, 2)
        self.c3 = card.Card(2, 4)
        self.c4 = card.Card(2, 5)
        self.c5 = card.Card(3, 7)
        self.c6 = card.Card(3, 8)
        self.c7 = card.Card(4, 10)
        self.c8 = card.Card(4, 11)

    # Initialize some Cards, and assign them a value and check that it worked
    def test_CardGetValue(self):
        # Testing that card value is initializing to current default value (0)
        self.assertEqual(0, self.c1.getValue())
        self.assertEqual(0, self.c2.getValue())
        self.assertEqual(0, self.c3.getValue())
        self.assertEqual(0, self.c4.getValue())
        self.assertEqual(0, self.c5.getValue())
        self.assertEqual(0, self.c6.getValue())
        self.assertEqual(0, self.c7.getValue())
        self.assertEqual(0, self.c8.getValue())
