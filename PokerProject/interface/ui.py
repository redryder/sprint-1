from PokerProject.interface import parseCommand, performCommand


class ui:

    def command(self, command):
        commandName = parseCommand.parseCommand(command)
        if commandName == "chip_amountCommand":
            return performCommand.chip_amountCommand(command)
