from django.contrib import admin

# Register your models here.
from .models import Player, Table, Entry

admin.site.register(Table)
admin.site.register(Player)
admin.site.register(Entry)
