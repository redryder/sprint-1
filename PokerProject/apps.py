from django.apps import AppConfig


class PokerprojectConfig(AppConfig):
    name = 'PokerProject'
