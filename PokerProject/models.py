from django.db import models
from django.contrib.auth.models import User

from django.core.validators import MaxValueValidator, MinValueValidator


#######################################
# This class is not done and is a WIP
########################################


class Entry(models.Model):
    message = models.CharField(max_length=100, default="")


class Card(models.Model):
    rank = models.CharField(max_length=1)
    suit = models.CharField(max_length=1)


class Player(models.Model):
    # ...
    username = models.CharField(max_length=30, default="")
    user = models.CharField(max_length=30, default="")
    cTable = models.CharField(max_length=30, default="")
    money = models.IntegerField(default=100)
    currentBet = models.IntegerField(default=0)
    cardsHeld = models.ManyToManyField(Card, blank=True, null=True)
    callHand = models.BooleanField(default=False)
    raiseHand = models.BooleanField(default=False)
    checkHand = models.BooleanField(default=False)
    foldHand = models.BooleanField(default=False)
    playerLost = models.BooleanField(default=False)
    myTurn = models.BooleanField(default=False)
    completeTurn = models.BooleanField(default=False)
    currentGame = models.BooleanField(default=False)


class Table(models.Model):
    # Name of the table
    nameTable = models.CharField(max_length=50)
    # Fields for the current and maximum number of players allowed
    num_players = models.IntegerField(default=0)
    free_spots = models.IntegerField(default=6)
    # # this ties players to a table
    players = models.ManyToManyField(Player, blank=True, null=True, related_name='players')
    InGame = models.ManyToManyField(Player, blank=True, null=True, related_name='InGame')
    cardDealt = models.BooleanField(default=False)
    highscore = models.IntegerField(default=0)
    highScoreName = models.ManyToManyField(Player, blank=True, null=True, related_name="high_score")
    # Boolean for game in progress
    currentGame = models.BooleanField(default=False)
    betGame = models.BooleanField(default=False)
    finishGame = models.BooleanField(default=False)
    currentPot = models.IntegerField(default=0)
    winner = models.ManyToManyField(Player, blank=True, null=True, related_name='Table_Winner')
    tableMessage = models.ManyToManyField(Entry, blank=True, null=True, related_name="Table_Log")
    playerActions = models.ManyToManyField(Entry, blank=True, null=True, related_name="Player_Log")
