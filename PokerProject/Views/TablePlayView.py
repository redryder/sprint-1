from django.views import View
from django.shortcuts import render
from django.contrib.auth import models
from PokerProject.models import Entry, Table, Player


# Still Work In Progress
# Need to Finish This
# Ace Vue - Working on It
class TablePlayView(View):
    playTable = None
    Allplayers = None
    listplayers = None

    def get(self, request):

        # get the player object
        curplayer = Player.objects.get(username=request.user.username)

        # get the current table player is playing at
        try:
            self.playTable = Table.objects.get(nameTable=curplayer.cTable)
            self.listplayers = self.playTable.players.all()
            self.Allplayers = self.listplayers
        except Table.DoesNotExist:
            return render(request, "main/InvalidTable.html")


        count = 0
        for x in self.playTable.InGame.all():
            if x.completeTurn == True:
                count = count + 1

        if count == len(self.playTable.InGame.all()):
            return render(request, "main/GameResultsView.html", {'table': self.playTable,
                                                       'players': self.listplayers, 'playerhand':
                                                           curplayer.cardsHeld.all, 'username':
                                                           request.user.username, 'player': curplayer})
        else:
            return render(request, "main/TablePlay.html", {'table': self.playTable,
                                                           'players': self.listplayers, 'playerhand':
                                                               curplayer.cardsHeld.all, 'username':
                                                               request.user.username, 'player': curplayer})

    def post(self, request):

        # get the player object
        curplayer = Player.objects.get(username=request.user.username)



        # get the current table player is playing at
        try:
            self.playTable = Table.objects.get(nameTable=curplayer.cTable)
            self.listplayers = self.playTable.players.all()
            self.Allplayers = self.listplayers
        except Table.DoesNotExist:
            return render(request, "main/InvalidTable.html")



        # check commands
        command = request.POST.get("command", None)
        bet = request.POST.get("bet", None)

        # value of the bet input
        numBet = 0

        # amount user bet is not a number then we set it to zero
        try:
            numBet = int(bet)

        except ValueError:
            numBet = 0
            player = request.user.username
            newmessage = Entry(message="The Bet Value entered by user: "
                                       "[" + player + "] is invalid. "
                                                      "Set bet to: 0")
            newmessage.save()
            self.playTable.tableMessage.add(newmessage)
            self.playTable.save()

        # if user bet less than 0, set it to 0
        if numBet < 0:
            numBet = 0
            player = request.user.username
            newmessage = Entry(message="The Bet Value entered by user: "
                                       "[" + player + "] is Less than 0! Set bet to: 0")
            newmessage.save()
            self.playTable.tableMessage.add(newmessage)
            self.playTable.save()

        # if user bet more than 5, set it to 5
        if numBet > 5:
            numBet = 5
            player = request.user.username
            newmessage = Entry(message="The Bet Value entered by user: "
                                       "[" + player + "] is Greater than 5! Set bet to: 5")
            newmessage.save()
            self.playTable.tableMessage.add(newmessage)
            self.playTable.save()

        # find all the players
        self.Allplayers = self.playTable.InGame.all()

        # find all the winners!
        playerwinners = self.playTable.winner.all()

        # initialize local variable
        winnerDeterminer = None
        loserDeterminer = None

        # If the user is one of the winners we will go ahead and update Player.
        try:
            # fetch the winner
            winnerDeterminer = playerwinners.get(user=request.user.username)

            #Complete the turn after user command is processed.
            winnerDeterminer.completeTurn = True

            #save changes
            winnerDeterminer.save()

            # if the winner chose to raise. The money raise will be added to the current table pot and the total
            # will be added to the player. Max Raise [5] -- !
            # For Now Sprint 1 only asks for one betting session so Raise will behave linear. Players do NOT
            # yet have the ability to call a raise or to raise a raise.
            # Will need to update in the future after Sprint 1 to allow players to respond to a Raise!
            # Additionally, a raise can be countered by another raise! More features will be added later.
            if command == "Raise" or command == "raise":

                #Not Enough Chips To Bet, the Max Chip Possible
                if winnerDeterminer.money < numBet + self.playTable.currentPot:
                    numBet = winnerDeterminer.money
                    # give the player the winning pot
                    winnerDeterminer.money = winnerDeterminer.money + numBet
                else:

                    # give the player the winning pot plus the raise bet.
                    winnerDeterminer.money = winnerDeterminer.money + numBet + self.playTable.currentPot

                # set the current bet
                winnerDeterminer.currentBet = numBet + self.playTable.currentPot

                # set boolean for raising hand to true
                winnerDeterminer.raiseHand = True

                # set boolean for completing 1 betting session to true
                winnerDeterminer.completeTurn = True

                # save the model
                winnerDeterminer.save()

                # Since the player raised, the table pot needs to add the bet.
                self.playTable.currentPot = self.playTable.currentPot + numBet
                self.playTable.save()

                # This is the amount the player wins. We need to Update Table High Score
                # If the score won by raise of the player is greater than current High Score. Set high score to player.
                # If player raise is equal to current High Score simply add the player to the list of High Scorers.
                # If player raise is less than the current High Score keep the current high score.
                if winnerDeterminer.currentBet > self.playTable.highscore:
                    # update table highscore bet to be the call bet
                    self.playTable.highscore = winnerDeterminer.currentBet

                    # update highscore name(s)
                    self.playTable.highScoreName.clear()
                    self.playTable.highScoreName.add(winnerDeterminer)

                    # save changes
                    self.playTable.save()

                if winnerDeterminer.currentBet == self.playTable.highscore:
                    # Add the player to the list of high score(s)
                    self.playTable.highScoreName.add(winnerDeterminer)

                    # save changes
                    self.playTable.save()

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Raised : [" + str(numBet) + "]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Raised : [" + str(numBet) + "]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()


            # if the player calls he/she will bet whatever the current bet raise was before the player's turn. [MAX: 5]
            # -- Will need to update in the future after Sprint 1 to allow players to call multiple times for
            # multiple betting sessions.
            elif command == "Call" or command == "call":

                # Check to see if the money player has is enough to call the current table pot. If not, the player only
                # receives the maximum about of gain as the player can afford.
                if winnerDeterminer.money < self.playTable.currentPot:

                    # set the current bet so we know how much the player called up to the player's turn.
                    # this is the limited amount the player won.
                    winnerDeterminer.currentBet = winnerDeterminer.money

                    # set the win to a minimum allowed
                    winnerDeterminer.money = winnerDeterminer.money + winnerDeterminer.money

                else:

                    # if the player does have a winning hand go ahead and update chips with the current pot.
                    winnerDeterminer.money = winnerDeterminer.money + self.playTable.currentPot

                    # set the current bet so we know how much the player called up to the players turn.
                    # this is the actual pot the player loss.
                    winnerDeterminer.currentBet = self.playTable.currentPot

                # set boolean for calling a hand to true.
                winnerDeterminer.callHand = True

                # player has successfully finish betting 1st betting session.
                winnerDeterminer.completeTurn = True

                # update and save the player model.
                winnerDeterminer.save()

                # This is the amount the player wins. We need to Update Table High Score If the score won by call of
                # the player is equal to current High Score, add player to high score to player. If player raise is
                # less than the current High Score keep the current high score.
                if winnerDeterminer.currentBet > self.playTable.highscore:
                    # update table highscore bet to be the call bet
                    self.playTable.highscore = winnerDeterminer.currentBet

                    # update highscore name(s)
                    self.playTable.highScoreName.clear()
                    self.playTable.highScoreName.add(winnerDeterminer)

                    # save changes
                    self.playTable.save()

                if winnerDeterminer.currentBet == self.playTable.highscore:
                    # Add the player to the list of high score(s)
                    self.playTable.highScoreName.add(winnerDeterminer)

                    # save changes
                    self.playTable.save()

                currentpot = str(self.playTable.currentPot)

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Called : [" + currentpot + "]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()

                currentpot = str(self.playTable.currentPot)

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Called : [" + currentpot + "]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()


            # If the player checks, for now, [Sprint 1 - Requirements] --- Since this is the 1st betting session the
            # player will behave very linearly. If any of the players so far has raised or called,
            # check is technically not a legal command. However, since Sprint 1 is only requiring one betting session we will
            # treat "Check" like a "Call" for now. The player's hand is displayed after the game ends but NO chips
            # will be awarded to the player that choose to "Check" the hand.
            # More functionality will be added after Sprint 1.
            # Check will receive more features and functionality after Sprint 1.
            # For Sprint 1 only one betting session is required.
            # So a "Check" after a "Call" or "Raise" will be ignored for now. Player reveals hand at the end.
            elif command == "Check" or command == 'check':

                # Player will display hand in the end but not receive any chips
                winnerDeterminer.checkHand = True
                winnerDeterminer.save()

                # set boolean for completing 1 betting session to true
                winnerDeterminer.completeTurn = True

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Check : [Hand will be shown. No chips awarded!]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()

                # Let everyone know the table message
                tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                ', Check: Hand')
                tableNewMessage.save()
                self.playTable.tableMessage.add(tableNewMessage)
                self.playTable.save()

            # Since the input given is not valid or the player input "Fold" as a command, the hand will be hidden and
            # not displayed. With a full betting session of multiple betting rounds, a player could have bet in
            # earlier rounds but decide to fold in later rounds. Due to the scope of Sprint 1, since only 1 betting
            # session is inputted the Player naturally loses no chip and show no hands to the table.

            elif command == "Fold" or command == 'fold':

                # set boolean for completing 1 betting session to false
                winnerDeterminer.completeTurn = True
                winnerDeterminer.foldHand = True

                # set boolean for losing
                winnerDeterminer.playerLost = True

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Fold : "
                                                                      "[Hand will NOT be shown. No chips awarded!]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()

                # Let everyone know the table message
                tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                ', Fold: Hand')
                tableNewMessage.save()
                self.playTable.tableMessage.add(tableNewMessage)
                self.playTable.save()

            else:

                # set boolean for completing 1 betting session to false
                winnerDeterminer.completeTurn = True

                # set boolean for losing
                winnerDeterminer.playerLost = True

                # Let everyone know what the player did via text.
                playerTookAction = Entry(
                    message="The Player: [" + request.user.username + "] Your Input Command Was Invalid! Hand Folded"
                                                                      "[Hand will NOT be shown. No chips awarded!]")
                playerTookAction.save()
                self.playTable.playerActions.add(playerTookAction)
                self.playTable.save()

                # Let everyone know the table message
                tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                ', Your Input Was Invalid. Hand Folded.')
                tableNewMessage.save()
                self.playTable.tableMessage.add(tableNewMessage)
                self.playTable.save()

        # If the Player is one of the losers we will go ahead and update Player respectively.
        except Player.DoesNotExist:
            loserDeterminer = Player.objects.get(username=request.user.username)
            loserDeterminer.currentBet = numBet
            loserDeterminer.playerLost = True
            loserDeterminer.save()

            if loserDeterminer.completeTurn == False:

                loserDeterminer.completeTurn = True
                loserDeterminer.save()
                # If the loser chose to raise, the money raise will be subtracted from the player. Max Raise [5] -- !
                # For Now Sprint 1 only asks for one betting session so Raise will behave linear. Players do NOT
                # yet have the ability to call a raise or raise a raise.
                # Will need to update in the future after Sprint 1 to allow players to respond to a Raise!
                # Additionally, a raise can be countered by another raise! More features will be added later.
                if command == "Raise" or command == "raise":

                    # Not Enough Chips To Bet, the Max Chip Possible
                    if loserDeterminer.money < numBet + self.playTable.currentPot:
                        numBet = loserDeterminer.money
                        # deduct the player the amount of the winning pot proportional to amount allowed
                        loserDeterminer.money = winnerDeterminer.money - numBet
                    else:
                        # deduct the full loss from loser
                        loserDeterminer.money = loserDeterminer.money - numBet - self.playTable.currentPot

                    # set boolean for raising hand to true
                    loserDeterminer.raiseHand = True

                    # set boolean for completing 1 betting session to true
                    loserDeterminer.completeTurn = True

                    # set boolean for losing
                    loserDeterminer.playerLost = True

                    # save the model
                    loserDeterminer.save()

                    # Since the player raised, the table pot needs to add the bet.
                    self.playTable.currentPot = self.playTable.currentPot + numBet
                    self.playTable.save()

                    # Let everyone know what the player did via text.
                    playerTookAction = Entry(
                        message="The Player: [" + request.user.username + "] Raised : [" + str(numBet) + "]")
                    playerTookAction.save()
                    self.playTable.playerActions.add(playerTookAction)
                    self.playTable.save()

                    # Let everyone know the table message
                    tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                    ', Raise: ' + str(numBet))
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()


                # if the player calls he/she will bet whatever the current bet raise was before the player's turn. [MAX: 5]
                # if the chips player holds is less than the current pot bet, the command "Call" will
                # Will need to update in the future after Sprint 1 to allow players to call multiple times for
                # multiple betting sessions.
                elif command == "Call" or command == "call":

                    # Check to see if the money player has is enough to call the current table pot. If not, the player only
                    # receives the maximum about of deduction as the player can afford.
                    if loserDeterminer.money < self.playTable.currentPot:

                        # set the loss to a minimum allowed
                        loserDeterminer.money = loserDeterminer.money - winnerDeterminer.money

                        # set the current bet so we know how much the player called up to the player's turn.
                        # this is the limited amount the player loss.
                        loserDeterminer.currentBet = loserDeterminer.money

                    else:

                        # the player does have a losing hand and enough chip go ahead and update chips with the current pot.
                        loserDeterminer.money = loserDeterminer.money - self.playTable.currentPot

                        # set the current bet so we know how much the player called up to the players turn.
                        # this is the actual pot the player loss.
                        loserDeterminer.currentBet = self.playTable.currentPot

                    # set boolean for calling a hand to true.
                    loserDeterminer.callHand = True

                    # player has successfully finish betting 1st betting session.
                    loserDeterminer.completeTurn = True

                    # set boolean for losing
                    loserDeterminer.playerLost = True

                    # update and save the player model.
                    loserDeterminer.save()

                    currentpot = str(self.playTable.currentPot)

                    # Let everyone know what the player did via text.
                    playerTookAction = Entry(
                        message="The Player: [" + request.user.username + "] Called : [" + currentpot + "]")
                    playerTookAction.save()
                    self.playTable.playerActions.add(playerTookAction)
                    self.playTable.save()

                    # Let everyone know the table message
                    tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                    ', Call: ' + str(loserDeterminer.currentBet))
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()

                elif command == "Check":

                    # Player will display hand in the end but not receive any chips
                    loserDeterminer.checkHand = True
                    loserDeterminer.save()

                    # set boolean for completing 1 betting session to true
                    loserDeterminer.completeTurn = True

                    # Let everyone know what the player did via text.
                    playerTookAction = Entry(
                        message="The Player: [" + request.user.username + "] Check : [Hand will be shown. No chips awarded!]")
                    playerTookAction.save()
                    self.playTable.playerActions.add(playerTookAction)
                    self.playTable.save()

                    # Let everyone know the table message
                    tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                    ', Check: Hand')
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()

                # If the player checks, for now, [Sprint 1 - Requirements] --- Since this is the 1st betting session the
                # player will behave very linearly. If any of the players so far has raised or called,
                # check is technically not a legal command. However, since Sprint 1 is only requiring one betting session we will
                # treat "Check" like a "Call" for now. The player's hand is displayed after the game ends but NO chips
                # will be awarded to the player that choose to "Check" the hand.
                # More functionality will be added after Sprint 1.
                # Check will receive more features and functionality after Sprint 1.
                # For Sprint 1 only one betting session is required.
                # So a "Check" after a "Call" or "Raise" will be ignored for now. Player reveals hand at the end.
                elif command == "Fold" or command == "fold":

                    # Player will display hand in the end but not receive any chips
                    loserDeterminer.foldHand = True
                    loserDeterminer.save()

                    # set boolean for completing 1 betting session to true
                    loserDeterminer.completeTurn = True

                    # set boolean for losing
                    loserDeterminer.playerLost = True

                    # Let everyone know what the player did via text.
                    playerTookAction = Entry(
                        message="The Player: [" + request.user.username + "] Fold : [Hand will be hidden. No chips awarded!]")
                    playerTookAction.save()
                    self.playTable.playerActions.add(playerTookAction)
                    self.playTable.save()

                    # Let everyone know the table message
                    tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                    ', Fold: Hand')
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()

                # Since the input given is not valid
                # Ask Player for input again.
                else:
                    # set boolean for completing 1 betting session to true
                    loserDeterminer.completeTurn = True

                    # set boolean for losing
                    loserDeterminer.playerLost = True

                    # Let everyone know what the player did via text.
                    playerTookAction = Entry(
                        message="The Player: [" + request.user.username + "] Invalid Input : [Hand will NOT be shown. No chips awarded!]")
                    playerTookAction.save()
                    self.playTable.playerActions.add(playerTookAction)
                    self.playTable.save()

                    # Let everyone know the table message
                    tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                    ', Your Input Was Invalid Your Hand Folded.')
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()

                # find the players
                listplayers = self.playTable.InGame.all()

                # find next player to go next
                for allplay in listplayers:

                    # if player did not go yet, let the player go ahead and go
                    if allplay.myTurn == False:
                        allplay.myTurn = True
                        allplay.save()
                        self.playTable.betGame = True
                        self.playTable.save()
                        break

                # Since a game has started and we have players taking turn. We need to see if all players have gone.
                # If every player has gone we will redirect to winning page!

                # finish the player's turn. Only one betting session
                playerUser = Player.objects.get(username=request.user.username)
                playerUser.completeTurn = True
                playerUser.save()

            else:

                tableNewMessage = Entry(message='Player: ' + request.user.username +
                                                ', Your Input Is Ignored You Already Took And Action!')
                tableNewMessage.save()
                self.playTable.tableMessage.add(tableNewMessage)
                self.playTable.save()

        # keep track of players that already took action
        gamecomplete = 0

        listplayers = self.playTable.InGame.all()
        # calculate number of players that already completed their turn(s)
        for allplay in listplayers:
            if allplay.completeTurn == True:
                gamecomplete = gamecomplete + 1

        # If every player has gone we will now determine the winner and go to the final winning page!
        # The logic is written here for determining the winner.
        # The result will be rendered to GameResultsView.html
        if gamecomplete == len(listplayers.all()):
            # Find the current user to render
            searchplayer = self.playTable.InGame.get(username=request.user.username)
            # Get the user hand
            playerhand = searchplayer.cardsHeld.all()

            # Let everyone know the game has completed.
            tableNewMessage = Entry(message="The Game Has Completed!")
            tableNewMessage.save()
            self.playTable.tableMessage.add(tableNewMessage)
            self.playTable.save()
            self.playTable.finishGame = True

            return render(request,
                          "main/GameResultsView.html", {'table': self.playTable,
                                                             'players': self.Allplayers, 'playerhand':
                                                                 playerhand, 'username': request.user.username,
                                                             'player': searchplayer})
        else:

            searchplayer = self.playTable.InGame.get(username=request.user.username)

            # Get the user hand
            playerhand = searchplayer.cardsHeld.all()

            # find all the players that are in the game
            listplayers = self.playTable.InGame.all()

            # search for every player in order to see if they have gone yet
            for allplay in listplayers:

                if allplay.myTurn == False:
                    allplay.myTurn = True
                    allplay.save()

                    # Start the betting phase

                    # Set betting phase to start
                    self.playTable.betGame = True
                    self.playTable.save()

            return render(request, "main/TablePlay.html", {'table': self.playTable, 'allplay': allplay,
                                                          'players': self.Allplayers.all(), 'playerhand':
                                                              playerhand, 'username':
                                                              request.user.username, 'player': searchplayer})
