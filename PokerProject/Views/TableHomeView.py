from django.contrib.auth import models
from django.views import View
from django.shortcuts import render

from PokerProject.classes.deck import Deck
from PokerProject.classes.poker import Poker
from PokerProject.models import Entry, Table, Player, Card


# Still Work In Progress
# Need to Finish This
# Ace Vue - Working on It
class TableHomeView(View):
    tablename = None
    Allplayers = None
    playTable = None
    hands = None
    player = None

    def get(self, request):

        # find all the table objects in the database
        tables = Table.objects.all()

        #fetch player
        try:
            self.player = Player.objects.get(username=request.user.username)
        except Player.DoesNotExist:
            return render(request, 'main/InvalidUser.html')

        #if the player has been removed from the table in prior game direct the player to home screen.
        try:
            self.playTable = Table.objects.get(nameTable=self.player.cTable)
        except Table.DoesNotExist:
            return render(request, 'main/TableView.html', {'tables': tables, 'user': self.player})

        self.Allplayers = self.playTable.players.all()

        return render(request, 'main/TableView.html', {'table': self.playTable, 'playerlist': self.Allplayers,
                                                       'player': self.player, 'players': self.Allplayers})

    def post(self, request):

        # locate the name of the table
        tablename1 = request.POST.get("tablename", None)

        # find all the table objects in the database
        tables = Table.objects.all()

        # find the player object
        self.player = Player.objects.get(user=request.user.username)

        tablefound = None

        # check to see if the table entered by player can be found at the table
        try:
            # find the table
            tablefound = tables.get(nameTable=tablename1)
            self.tablename = tablefound.nameTable
            self.playTable = tablefound
            self.Allplayers = tablefound.players.all()

            # if the table is full let the player know and don't join the player to the table
            if tablefound.num_players >= 6:
                return render(request, 'main/TableFullView.html')

        # if table does not exist tell the user the table is invalid
        except Table.DoesNotExist:
            return render(request, 'main/InvalidTable.html')

        count = 0
        for x in self.playTable.InGame.all():
            if x.completeTurn == True:
                count = count + 1

        if count == len(self.playTable.InGame.all()) and self.player.currentGame is True:
            return render(request, "main/GameResultsView.html", {'table': self.playTable,
                                                                 'players': self.playTable.players, 'username':
                                                                     request.user.username, 'player': self.player})


        #check to see if player is already in a game
        if self.player.currentGame == True:

            # Get the user hand
            playerhand = self.player.cardsHeld.all()

            # find all the players that are in the game
            listplayers = self.playTable.InGame.all()

            # search for every player in order to see if they have gone yet
            for allplay in listplayers:

                if allplay.myTurn == False:
                    allplay.myTurn = True
                    allplay.save()

                    # Set betting phase to start
                    self.playTable.betGame = True
                    self.playTable.save()

            return render(request, "main/TablePlay.html", {'table': self.playTable,
                                                           'players': self.Allplayers.all(), 'playerhand':
                                                               playerhand, 'username':
                                                               request.user.username, 'player': self.player})

        #check to see if player is already at the table
        try:
            # try to find the player at the table.
            self.player = tablefound.InGame.get(username=self.player.username)

            #since the player is found at the table return the player to table play
            return render(request, 'main/TablePlay.html')

        except Player.DoesNotExist:
            # Since the player selected the table and not currently sitting at the table we will add the player to the table
            tablefound.players.add(self.player)
            tablefound.num_players = tablefound.num_players + 1
            tablefound.free_spots = tablefound.free_spots - 1
            tablefound.save()

            # add the table pointer. Player chooses to join this table.
            self.player.cTable = tablefound.nameTable
            self.player.currentGame = True
            # save
            self.player.save()

            # Message the table to update everyone that this player joined the table.
            tableNewMessage = Entry(message='Player: [' + self.player.username + "] joined the Table!")
            tableNewMessage.save()
            tablefound.tableMessage.add(tableNewMessage)
            tablefound.save()

            # Save the Table updates
            tablefound.save()

            # Ensure the player is now pointing to the table.
            self.player.cTable = tablefound.nameTable
            self.player.save()

        # If more than 2 players are sitting at the table and a current game is going on. Let the user know to wait
        # in queue for next game.
        if tablefound.num_players >= 2 and tablefound.currentGame is True and self.player.currentGame is True:

            # There is a current active game going on. We will NOT reset the game.
            if tablefound.betGame == True:

                # search for every player in order to see if they have gone yet

                # find the players
                listplayers = tablefound.InGame.all()

                # find next player to go next
                for allplay in listplayers:

                    # if player did not go yet, let the player go ahead and go
                    if allplay.myTurn == False:

                        # update player to have player go next.
                        allplay.myTurn = True
                        allplay.save()
                        tablefound.betGame = True
                        tablefound.save()
                        break

                # Since a game has started and we have players taking turn. We need to see if all players have gone.
                # If every player has gone we will redirect to winning page!

                # keep track of players that already took action
                gamecomplete = 0

                # calculate number of players that already completed their turn(s)
                for allplay in listplayers:
                    if allplay.completeTurn == True:
                        gamecomplete = gamecomplete + 1

                # If every player has gone we will now determine the winner and go to the final winning page!
                # The logic is written here for determining the winner.
                # The result will be rendered to GameResultsView.html
                if gamecomplete == len(listplayers.all()):
                    # Find the current user to render
                    # Get the user hand
                    # Let everyone know the game has completed.
                    # Find the current user to render
                    # Get the user hand

                    # Let everyone know the game has completed.
                    tableNewMessage = Entry(message="The Game Has Completed!")
                    tableNewMessage.save()
                    self.playTable.tableMessage.add(tableNewMessage)
                    self.playTable.save()


                    # Go To Results HTML page
                    return render(request, "main/GameResultsView.html", {'table': self.playTable,
                                                    'players': self.Allplayers, 'username':
                                        request.user.username, 'avatar': request.user.username,'player': self.player})

                # The game did not finish yet and we'll have to keep waiting and asking for next player for input
                else:
                    searchplayer = None
                    playerhand = None
                    try:
                        # Find the current user to render
                        searchplayer = tablefound.InGame.get(username=request.user.username)
                        # Get the user hand
                        playerhand = searchplayer.cardsHeld.all()
                    except Player.DoesNotExist:
                        self.player.currentGame = False
                        self.player.save()
                        #player is spectating this game.
                        return render(request,
                                      "main/TablePlay.html", {'table': tablefound,
                                                              'players': self.Allplayers, 'playerhand':
                                                                  playerhand, 'username': request.user.username,
                                                              'avatar': request.user.username, 'player': self.player})
                    # Go back and ask for or wait for current player's input.
                    return render(request,
                                  "main/TablePlay.html", {'table': tablefound,
                                                          'players': self.Allplayers, 'playerhand':
                                                              playerhand, 'username': request.user.username,
                                                          'avatar': request.user.username,'player': self.player})

            # Current Game Already Exists. Just let everyone know.
            else:

                # create message
                tablefound.currentGame = True
                tableNewMessage = Entry(message='Current Game Exists. Please wait to be Dealt in ')
                tableNewMessage.save()
                tablefound.tableMessage.add(tableNewMessage)

                # save message entry
                tablefound.save()
                # find all the players
                playerlist = tablefound.players.all()
                return render(request, 'main/TableView.html',
                              {'table': self.playTable, 'player':
                                  self.player,
                               'currentGame': tablefound.currentGame})

        # Game is going to either start because we now have 2 players, or game has already ended and we are going to
        # Restart a new game!
        if tablefound.num_players >= 2 and tablefound.currentGame is False:

            # If cards have not been dealt yet. Deal cards.
            if tablefound.cardDealt == False:

                #we need to add players at the table to the subset of players playing a new game.
                self.playTable.InGame.set(self.playTable.players.all())
                self.playTable.save()

                # Mark to ensure we know a game is currently going on.
                # We officially start or restart a game
                tablefound.currentGame = True

                # Deal only once if we have to refresh the form
                tablefound.cardDealt = True

                # Make sure players know they are in the game.
                for i in self.playTable.InGame.all():
                    i.currentGame = True
                    i.save()

                # Update activity log for the Table
                # Note:Player Activity logs will be listed on html
                tableNewMessage = Entry(message='A New Game Has Started!')
                tableNewMessage.save()
                tablefound.tableMessage.add(tableNewMessage)
                tablefound.save()

                # Find all the players currently sitting at the table
                playerlist = tablefound.players.all()

                # Set all the players sitting at the table to a subset of players. This final copy of players are the ones
                # that got into the game. Everyone else in or out of table must wait.
                tablefound.InGame.set(playerlist)
                tablefound.save()

                # Find all the players currently sitting at the table
                playerlist = tablefound.players.all()

                # Set all the players sitting at the table to a subset of players. This final copy of players are the ones
                # that got into the game. Everyone else in or out of table must wait.
                for h in playerlist:
                    tablefound.InGame.add(h)
                    tablefound.save()

                # Find all the players that are playing the game.
                self.Allplayers = self.playTable.InGame.all()

                # Create a poker game simulation with proper number of hands
                pokergame = Poker(len(self.Allplayers.all()))

                # Play the game to determine the value of each hands
                pokergame.play()

                # The hands of poker are looked at
                hands = pokergame.hands

                # grab the numeric point values of each hand
                numericHand = []

                #determine the winning hands of the players.
                for zz in hands:

                    check = 0

                    test = pokergame.isRoyal(zz)
                    if test[0] is True:
                        print('it is royal flush')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isStraightFlush(zz)
                    if test[0] is True and check != 1:
                        print('it is royal str8flush')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isFour(zz)
                    if test[0] is True and check != 1:
                        print('it is royal four of kind')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isFull(zz)
                    if test[0] is True and check != 1:
                        print('it is royal full house')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isFlush(zz)
                    if test[0] is True and check != 1:
                        print('it is flush')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isStraight(zz)
                    if test[0] is True and check != 1:
                        print('it is straight')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isThree(zz)
                    if test[0] is True and check != 1:
                        print('it is three')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isTwo(zz)
                    if test[0] is True and check != 1:
                        print('it is two')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isOne(zz)
                    if test[0] is True and check != 1:
                        print('it is one')
                        print('points added to hand:' + str(test[1]))
                        check = 1
                        numericHand.append(test[1])

                    test = pokergame.isHigh(zz)
                    if test[0] is True and check != 1:
                        print('it is high')
                        print('points added to hand:' + str(test[1]))
                        numericHand.append(test[1])




                #The Winning Point hand is found!
                winningHand = max(numericHand)
                print("max hand is:" + str(winningHand))

                count = 0
                for i in numericHand:
                    print("points added to hand: " + str(count) + 'is :' + str(i))

                    count = count + 1



                count1 = 0
                # Go through all the players at the table and deal them cards until everyone has 5 cards.
                for i in self.playTable.InGame.all():

                    # Each hand generated is handed off to each player.
                    # The hands are broken down into card strings which are constructed to Card model
                    # and added to a player model's current hand.
                    aa = hands[count1]

                    for z in aa:
                        cardDrawnRank = z.__str__()
                        cardDrawnSuit = z.suit

                        CardModel = Card.objects.create(rank=cardDrawnRank, suit=cardDrawnSuit)
                        CardModel.save()
                        i.cardsHeld.add(CardModel)
                        i.save()


                    # If the player's hand is one of the winning hand we let the Table Model know so
                    # That the best hand can be awarded accurately.
                    bb = numericHand[count1]
                    print("for player: " + i.username + " hand numeric points: " + str(bb))
                    if bb == winningHand:
                        print("for player: " + i.username + " winning hand numeric points: " + str(bb))
                        self.playTable.winner.add(i)
                        self.playTable.save()
                    count1 = count1 + 1

                searchplayer = self.Allplayers.get(user=request.user.username)
                playerhand = searchplayer.cardsHeld.all()

                # Table message to Players before updating view
                tableNewMessage = Entry(message='A Cards Have Been Dealt to Players')
                tableNewMessage.save()
                tablefound.tableMessage.add(tableNewMessage)
                tablefound.save()

                # Start the betting phase
                tableNewMessage = Entry(message='Starting the Betting Phase')
                tableNewMessage.save()
                tablefound.tableMessage.add(tableNewMessage)
                tablefound.save()


            # Get the user hand
            playerhand = self.player.cardsHeld.all()

            # find all the players that are in the game
            listplayers = tablefound.InGame.all()

            # search for every player in order to see if they have gone yet
            for allplay in listplayers:

                if allplay.myTurn == False and allplay.currentGame == True:
                    allplay.myTurn = True
                    allplay.save()

                    # Start the betting phase

                    #Set betting phase to start
                    tablefound.betGame = True
                    tablefound.save()

                    Iplay = True
                    return render(request,
                                  "main/TablePlay.html", {'table': tablefound, 'Iplay': Iplay,
                                                          'players': self.Allplayers.all(), 'playerhand':
                                                              playerhand, 'username':
                                                              request.user.username, 'player': self.player})

            Iplay = False
            return render(request,
                          "main/TablePlay.html", {'table': tablefound,
                                                  'players': self.Allplayers.all(), 'playerhand':
                                                      playerhand, 'Iplay': Iplay, 'username':
                                                      request.user.username, 'player': self.player})

        if tablefound.num_players < 2:
            tableNewMessage = Entry(message='There is NOT enough Players at the Table. Please Wait.')
            tableNewMessage.save()
            tablefound.tableMessage.add(tableNewMessage)
            tablefound.currentGame = False
            tablefound.save()

        return render(request, 'main/TableView.html',
                          {'table': tablefound,
                              'player': self.player, 'currentGame': tablefound.currentGame})

