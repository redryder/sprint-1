from django.contrib.auth import models
from django.views import View
from django.shortcuts import render

from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class HighScoreView(View):
    table_service = None

    def get(self, request):
        self.table_service = Table.objects.all()

        table = []
        score = []
        counter = []

        for tables in self.table_service:
            table.append(tables.nameTable)
            score.append(tables.highscore)                  #create the scores with table, players, and score
            counter.append(tables.nameTable)

        querySetEntry = Entry.objects.all()
        listEntry = list(querySetEntry)

        tables = Table.objects.all()
        tables = list(tables)

        # for t in highScores:
        # print(t.highScoreName + " " + str(t.highScore))
        if request.user.is_superuser:
            return render(request, 'main/HighScoreView.html', {"list": listEntry, "tables": tables,         #send to HTMl file with inputs
                                                          "counter": counter, "TableName": table, "HighScore": score})
        else:
            return render(request, 'main/NotSuperuserError.html')               #when not a superuser, send error message

    def post(self, request):

        #Pull Table High Score Reset Name
        tableReset = request.POST.get('tableName', None)

        try:

            #Check if table exists
            tableUpdate = Table.objects.get(nameTable=tableReset)

            # set to no high score player for default
            tableUpdate.highscore = 0
            tableUpdate.highScoreName.clear()
            tableUpdate.save()

            self.table_service = Table.objects.all()

            table = []
            score = []
            counter = []

            for tables in self.table_service:                                   #for each table, prepare the highscore functions
                table.append(tables.nameTable)
                score.append(tables.highscore)
                counter.append(tables.nameTable)

            querySetEntry = Entry.objects.all()
            listEntry = list(querySetEntry)

            # Show Admin Views
            tables = Table.objects.all()
            tables = list(tables)

            # for t in highScores:
            # print(t.highScoreName + " " + str(t.highScore))
            return render(request, 'main/HighScoreView.html', {"list": listEntry, "tables": tables,
                                                          "counter": counter, "TableName": table, "HighScore": score})
        except Table.DoesNotExist:
            # the table needs to exist before high scores can be available
            return render(request, 'main/Admin_Invalid_HighScore_Reset.html')
