from django.contrib.auth import models
from django.views import View
from django.shortcuts import render

from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class CreateTableView(View):
    command = None
    command1 = None

    def get(self, request):
        players = Player.objects.all()
        tables = Table.objects.all()

        # for t in highScores:
        # print(t.highScoreName + " " + str(t.highScore))
        if request.user.is_superuser:
            return render(request, 'main/CreateTable.html',
                          {"tables": tables, "players": players})  # if player is a superuser they can create table
        else:
            return render(request,
                          'main/NotSuperuserError.html')  # if player is not a superuser send them to error page

    def post(self, request):
        self.command = request.POST.get("input", None)

        if self.command is not None and self.command != "":  # create the table with a name for it

            try:
                Table.objects.get(nameTable=self.command)
                return render(request, 'main/InvalidTable.html', {'user': request.user})
            except Table.DoesNotExist:
                newTable = Table(nameTable=self.command)
                newTable.save()

        self.command1 = request.POST.get("delete")

        if self.command1 is not None and self.command1 != "":
            try:
                alltable = Table.objects.all()
                tablefound = alltable.get(nameTable=self.command1)
                tablefound.delete()
            except Table.DoesNotExist:
                tables = Table.objects.all()
                listtables = list(tables)
                players = Player.objects.all()
                return render(request, 'main/CreateTable.html',
                              {"tables": listtables, "players": players})  # send superuser to the HTML link

        tables = Table.objects.all()
        listtables = list(tables)
        players = Player.objects.all()
        return render(request, 'main/CreateTable.html', {"tables": listtables, "players": players})  # send user to the refreshed HTML page
