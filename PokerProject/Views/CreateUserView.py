from django.contrib.auth import models
from django.contrib.auth.models import User
from django.views import View
from django.shortcuts import render

from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class CreateUserView(View):
    command = None
    command1 = None

    def get(self, request):
        players = Player.objects.all()
        tables = Table.objects.all()
        # for t in highScores:
        # print(t.highScoreName + " " + str(t.highScore))
        if request.user.is_superuser:
            return render(request, 'main/CreatePlayer.html',
                          {"players": players, "tables": tables})  # if player is a superuser they can create table
        else:
            return render(request,
                          'main/NotSuperuserError.html')  # if player is not a superuser send them to error page

    def post(self, request):
        if request.user.is_superuser:
            players = Player.objects.all()
            tables = Table.objects.all()
            self.command = request.POST.get("input", None)
            newPassword = request.POST.get("input2", None)

            if newPassword == "":
                return render(request,
                              'main/InvalidPassword.html')  # if player is not a superuser send them to error page

            if self.command == "":
                return render(request,
                              'main/InvalidUsername.html')  # if player is not a superuser send them to error page

            if self.command is not None and self.command != "":  # create the player with a name for it

                try:
                    Player.objects.get(username=self.command)
                    return render(request, 'main/InvalidUser.html', {'user': request.user})
                except Player.DoesNotExist:
                    newPlayer = Player(username=self.command, user=self.command)
                    newPlayer.save()
                    user = User.objects.create_user(username=self.command, password=newPassword)
                    user.save()

            self.command1 = request.POST.get("delete")

            if self.command1 is not None and self.command1 != "":
                try:
                    allPlayers = Player.objects.all()
                    playerfound = allPlayers.get(username=self.command1)
                    playerfound.delete()
                    user = User.objects.get(username=self.command1)
                    user.delete()
                except Player.DoesNotExist:
                    players = Player.objects.all()
                    tables = Table.objects.all()
                    return render(request, 'main/CreatePlayer.html',
                                  {"players": players, "tables": tables})  # send superuser to the HTML link

            players = Player.objects.all()
            tables = Table.objects.all()
            return render(request, 'main/CreatePlayer.html', {"players": players, "tables": tables})  # send user to the refreshed HTML page
        else:
            return render(request,
                          'main/NotSuperuserError.html')  # if player is not a superuser send them to error page