from django.contrib.auth import models
from django.views import View
from django.shortcuts import render

from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class PlayersNotEnoughView(View):

    def get(self, request):
        return render(request, "main/PlayersNotEnough.html")

    def post(self, request):
        return render(request, "main/PlayersNotEnough.html")