from django.contrib.auth import models
from django.contrib.auth.models import User
from django.views import View
from django.shortcuts import render
from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class MyView(View):

    def get(self, request):

        return render(request, 'registration/register.html')


    def post(self, request):

        username = request.POST.get("username", None)
        password = request.POST.get("password", None)

        if username == "" or password == "":
            return render(request, 'registration/registerError.html')

        if len(username) > 30 or len(password) > 30:
            return render(request, 'registration/registerError.html')

        try:
            Player.objects.get(username=username)
            return render(request, 'registration/registerError.html')
        except:
            pass

        try:
            User.objects.get(username=username)
            return render(request, 'registration/registerError.html')
        except:
            pass


        User.objects.create_user(username=username, email="email@email.com", password=password)
        Player.objects.create(username=username, user=username)

        return render(request, 'main/HomeView.html')