from django.contrib.auth import models
from django.contrib.auth.models import User
from django.views import View
from django.shortcuts import render
from PokerProject.classes.deck import Deck
from PokerProject.models import Entry, Table, Player


class MyView(View):

    def get(self, request):

        #if user is admin, show admin views and functionality
        if request.user.is_superuser:
            # Show Admin Views
            tables = Table.objects.all()
            tables = list(tables)
            players = Player.objects.all()
            players = list(players)

            return render(request, 'main/AdminView.html', {"tables": tables, "players": players})

        else:

            if request.user.is_authenticated:
                try:
                    #Player already exist and started playing don't initialize
                    #Instead, allow player to resume a started game
                    player = Player.objects.all().get(username=request.user.username)

                    #check to see if player is sitting at a table
                    try:
                        self.playTable = Table.objects.get(nameTable=player.cTable)
                        self.Allplayers = self.playTable.InGame
                        playerhand = player.cardsHeld

                        #If player is not in the current game at the table. Leave the player at the Table View.
                        if player.currentGame == False:
                            return render(request, "main/TablePlay.html", {'table': self.playTable,
                                                                               'players': self.Allplayers, 'playerhand':
                                                                                   playerhand,
                                                                               'username': request.user.username,
                                                                               'player': player})
                        #If player was updated to join a game or is currently in a game, show the Table Play View
                        else:
                            return render(request, "main/TablePlay.html", {'table': self.playTable,
                                                                                 'players': self.Allplayers, 'playerhand':
                            playerhand, 'username': request.user.username, 'player': player})

                    except Table.DoesNotExist:

                        # find objects and vairiables
                        players = Player.objects.all()
                        players = list(players)

                        # create full table object list
                        tables = Table.objects.all()
                        listtables = list(tables)

                        player = Player.objects.all().get(username=request.user.username)

                        # return and render
                        return render(request, 'main/PlayerView.html',
                                          {'player': player, "players": players,
                                           "tables": listtables, "user": request.user.username})

                except Player.DoesNotExist:
                    #If player does not exist, create a player object for the user.
                    newplayer = Player(user=request.user.username, username=request.user.username)
                    newplayer.save()

                    #find objects and vairiables
                    players = Player.objects.all()
                    players = list(players)

                    #create full table object list
                    tables = Table.objects.all()
                    listtables = list(tables)

                    #return and render
                    return render(request, 'main/PlayerView.html',
                                      {"players": players, "tables": listtables, "user": request.user.username})

            else:
                return render(request, 'main/HomeView.html')