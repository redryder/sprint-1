from django.contrib.auth import models
from django.views import View
from django.shortcuts import render
from PokerProject.classes.deck import Deck
from PokerProject.classes.card import Card
from PokerProject.classes.poker import Poker
from PokerProject.models import Entry, Table, Player


class GameResultsView(View):
    playerModel = None
    playTable = None

    def get(self, request):

        # fetch the player model
        try:
            self.playerModel = Player.objects.get(username=request.user.username)
        except Player.DoesNotExist:
            return render(request, "main/InvalidUser.html")

        try:
            self.playTable = Table.objects.get(nameTable=self.playerModel.cTable)

            # Reset Table Logic

            ####################################################
            # Remove Players From Table Before Next Game
            ####################################################

            # We Keep High Score Value
            # We Keep High Score Players

            # Reset the Table. The Game Has Completed!
            self.playTable.currentGame = False

            # Reset the Table betting phase
            self.playTable.betGame = False

            # Reset the Table finish a game phase
            self.playTable.finishGame = False

            # Reset Card Dealt boolean
            self.playTable.cardDealt = False

            # Reset the pot to 0 for next Game.
            self.playTable.currentPot = 0

            # Clear the subset of winners from the Table.
            self.playTable.winner.clear()

            # Fetch all the message for the table from prior game.
            tableMessages = self.playTable.tableMessage.all()

            # Clear the messages for the table from prior game.
            self.playTable.tableMessage.clear()

            # Delete all the entry messages made for prior game for the table.
            for i in tableMessages:
                i.delete()

            # Fetch all the message for the player actions from prior game.
            playerMessages = self.playTable.tableMessage.all()

            # Clear the messages for the player Actions from prior game.
            self.playTable.playerActions.clear()

            # Delete all the entry messages made for prior game for the players.
            for p in playerMessages:
                p.delete()

            ####################################################
            # Reset All The Fields Used to Play a Game of Poker for the Players
            ####################################################

            for player in self.playTable.InGame.all():

                # Disconnect the Player from the table
                player.cTable = ""

                # The game has finished. Reset the current bet field for the player.
                player.currentBet = 0

                # Discard all the cards held by the Player
                player.cardsHeld.clear()

                # Reset the Action Taken Boolean Flags for the Player.
                player.callHand = False
                player.raiseHand = False
                player.checkHand = False
                player.foldHand = False
                player.currentGame = False

                # Reset the Lost Boolean Value
                player.playerLost = False

                # Reset the player Turn Boolean Value
                player.myTurn = False

                # Reset the player Complete Turn Boolean Value
                player.completeTurn = False

                # Save All The Changes
                player.save()

            # Determine the number of players in past game
            numPlayers = 0
            for z in self.playTable.InGame.all():
                numPlayers = numPlayers + 1

            # Update and reset numeric count of players
            self.playTable.num_players = self.playTable.num_players - numPlayers
            self.playTable.free_spots = self.playTable.free_spots + numPlayers

            # Remove the players played in last game from the Table
            for i in self.playTable.InGame.all():
                self.playTable.players.remove(i)

            # Clear Table of Current Players
            self.playTable.InGame.clear()

            # Save all the changes at the Table
            self.playTable.save()

            # find objects and variables
            players = Player.objects.all()
            players = list(players)

            # create full table object list
            tables = Table.objects.all()
            listtables = list(tables)



            ####################################################
            # All Done! Remove Players From Table Before Next Game
            ####################################################

            #######################################################################################
            # All Done! Player will be dealt next hand and remain at Table. Table Reset Successful
            #######################################################################################

            # return and render
            return render(request, 'main/PlayerView.html',
                          {'player': self.playerModel, "players": players,
                           "tables": listtables, "user": request.user.username})

        except Table.DoesNotExist:

            # find objects and variables
            players = Player.objects.all()
            players = list(players)

            # create full table object list
            tables = Table.objects.all()
            listtables = list(tables)

            ####################################################
            # All Done! Remove Players From Table Before Next Game
            ####################################################

            #######################################################################################
            # All Done! Player will be dealt next hand and remain at Table. Table Reset Successful
            #######################################################################################

            # return and render
            return render(request, 'main/PlayerView.html',
                          {'player': self.playerModel, "players": players,
                           "tables": listtables, "user": request.user.username})