"""PokerProjectDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from PokerProject.Views import MainView, RegisterView
from PokerProject.Views import HighScoreView
from PokerProject.Views import CreateTableView
from PokerProject.Views import CreateUserView
from PokerProject.Views import TableHomeView
from PokerProject.Views import TablePlayView
from PokerProject.Views import PlayersNotEnough
from PokerProject.Views import GameResultsView
from django.urls import path, include

#each path represents a url follow the '/'. Those paths relay to the views listed for each
urlpatterns = [
  path('admin/', admin.site.urls),
  path('accounts/', include('django.contrib.auth.urls')),
  path('', MainView.MyView.as_view()),
  path("gameresult/", GameResultsView.GameResultsView.as_view()),
  path("playersnotenough/", PlayersNotEnough.PlayersNotEnoughView.as_view()),
  path("tableplay/", TablePlayView.TablePlayView.as_view()),
  path("highscoreview/", HighScoreView.HighScoreView.as_view()),
  path("createtable/", CreateTableView.CreateTableView.as_view()),
  path("createuser/", CreateUserView.CreateUserView.as_view()),
  path("tablehome/", TableHomeView.TableHomeView.as_view()),
  path("adminhome/", MainView.MyView.as_view()),
  path("register/", RegisterView.MyView.as_view())
]

