from django.test import TestCase

from PokerProject.models import Player, Table, Dealer


class TestCanSitAtTable(TestCase):

    def setUp(self):

        #Creates dealer model
        self.dealer1 = Dealer.object.create(username="dealer1")

        #Creates 7 player models
        self.player1 = Player.objects.create(username="player1")
        self.player2 = Player.objects.create(username="player2")
        self.player3 = Player.objects.create(username="player3")
        self.player4 = Player.objects.create(username="player4")
        self.player5 = Player.objects.create(username="player5")
        self.player6 = Player.objects.create(username="player6")
        self.player7 = Player.objects.create(username="player7")

        #Creates 3 new empty table models
        self.table1 = Table.objects.create(name="table1")
        self.table2 = Table.objects.create(name="table2")
        self.table3 = Table.objects.create(name="table3")

        pass


    # Tests that a player can sit down at an empty table by
    #  adding the player to the table, saving changes to
    #  the database, and then checking if a new object
    #  that pulls player information about that table
    #  in the data base is correct.
    def test_CanSitEmpty(self):

        # Adds player1 to table1
        self.ui.command("dealer1 table1 Add player1")

        # Gets testPlayer1 from the database which should be player1
        testPlayer1 = self.table1.players.get(id=1)

        # Checks if testPlayer1 actually is player1
        if(testPlayer1.name == "player1"):
            self.assertTrue(True)
        else:
            self.assertTrue(False)

        pass

    # Does a similar test as shown above, except with one extra player
    def test_CanSitMultiple(self):

        # Adds 2 players to an empty table2 and saves the info to database
        self.ui.command("dealer1 table2 Add player1")
        self.ui.command("dealer1 table2 Add player2")

        # Pulls from database, player1 and player2
        testPlayer1 = self.table2.players.get(id=1)
        testPlayer2 = self.table2.players.get(id=2)

        # Checks that the players pulled from database are player1 and player2
        if(testPlayer1.name == "player1" and testPlayer2.name == "player2"):
            self.assertTrue(True)
        else:
            self.assertTrue(False)
        pass


    # Tests that no more than 6 players can be seated at a table.
    #  Since the capacity is 6, the 7th player should not be added.
    def test_CanSitFull(self):

        # Tries to add 7 players to the table
        self.ui.command("dealer1 table3 Add player1")
        self.ui.command("dealer1 table3 Add player2")
        self.ui.command("dealer1 table3 Add player3")
        self.ui.command("dealer1 table3 Add player4")
        self.ui.command("dealer1 table3 Add player5")
        self.ui.command("dealer1 table3 Add player6")
        self.ui.command("dealer1 table3 Add player7")

        # Tries to get the 7th player from the table
        try:
            testPlayer7 = self.table3.players.get(id=7)
        except:
            print("Table doesn't have id 7.")


        # Checks that player7 was not gotten from the table
        if (testPlayer7.username == "player7"):
            self.assertTrue(False)
        else:
            self.assertTrue(True)
        pass
