from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table
from PokerProject.interface.ui import ui


class TestPostDealBet(TestCase):
    def setUp(self):
        self.ui = ui()


        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')

        self.Table1 = Table('Table1')

    def test_bet_timing(self):             #Once the hand is dealt, players can bet when it is their turn
        self.assertEqual(self.ui.command("Player1: bet 5"), "ERROR can't place bet yet")
        self.assertEqual(self.ui.command("Player2: bet 5"), "ERROR can't place bet yet")
        self.assertEqual(self.ui.command("Player3: bet 5"), "ERROR can't place bet yet")
        self.ui.command("Player1: bet 5")

    def test_max_bet(self):                 #players can only raise a max of 5, or a max of their chips, whichever is less
        self.assertEqual(self.ui.command("Player2: bet 6"), "ERROR bet must only raise 5 or less")
        self.assertEqual(self.ui.command("Player2: bet 60"), "ERROR bet must only raise 5 or less")


    def test_circlular_turn(self):          #After a player bets, the next player turn is in the circular motion
        self.assertEqual(self.ui.command("Player2: bet 5", "ERROR it is not your turn"))
        self.ui.command("Player1: bet 5")
        self.assertEqual(self.ui.command("Player3: bet 6", "ERROR it is not your turn"))
        self.assertEqual(self.ui.command("Player1: bet 7", "ERROR it is not your turn"))
        self.ui.command("Player2: bet 6")
        self.assertEqual(self.ui.command("Player2: bet 8", "ERROR it is not your turn"))
        self.assertEqual(self.ui.command("Player1: bet 9", "ERROR it is not your turn"))
        self.ui.command("Player3: call 6")
        self.assertEqual(self.ui.command("Player2: call 6", "ERROR it is not your turn"))
        self.ui.command("Player1: fold")
        self.assertEqual(self.ui.command("Player3: fold", "ERROR it is not your turn"))
