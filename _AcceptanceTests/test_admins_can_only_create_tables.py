from django.contrib.auth.models import User
from django.test import TestCase
from unittest import mock

class TestAdminsCanOnlyCreateTables(TestCase):

    def setUp(self):

        self.mockCreateTableView = mock.Mock()
        self.mockCreateTableView.get = self.get

        self.mockUser = User.objects.create_user("Bob", "Bob@gmail.com", "password")
        self.mockAdmin = User.objects.create_superuser("Alice", "Alice@gmail.com", "password")

        self.request1 = mock.Mock()
        self.request1.user = self.mockUser

        self.request2 = mock.Mock()
        self.request2.user = self.mockAdmin

    def test_admin_can_create_user_cannot_create(self):

        self.assertEqual("/NotSuperuserError.html", self.mockCreateTableView.get(self.request1))
        self.assertEqual("/CreateTable.html", self.mockCreateTableView.get(self.request2))

    def get(self, request):

        if request.user.is_superuser:
            return "/CreateTable.html"
        else:
            return "/NotSuperuserError.html"
