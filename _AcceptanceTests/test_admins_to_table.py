from django.test import TestCase
from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table


class TestAdmins(TestCase):
    def setUp(self):

        self.Player1 = Player('Player1')                                #create players that can be tested
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')
        self.Player4 = Player('Player4')
        self.Player5 = Player('Player5')
        self.Player6 = Player('Player6')
        self.Player7 = Player('Player7')
        self.Player8 = Player('Player8')
        self.Player9 = Player('Player9')
        self.Player10 = Player('Player10')
        self.Player11 = Player('Player11')

        self.Table1 = Table('Table1')
        self.Table2 = Table('Table2')
        self.Table3 = Table('Table3')

    # Response to any command should include the list of tables (number of seats at each table)
    def test_tableList(self):
        pass

    # The admin can create users and tables (see test_admins_to_players.py), optionally delete as well (right here)
    # It should also display to the user what is happening
    def test_validPlayerDelete(self):
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player2"), 'the User <Player2> removed')
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player3"), 'the User <Player3> removed')
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player4"), 'the User <Player4> removed')
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player5"), 'the User <Player5> removed')

    def test_invalidPlayerDelete(self):
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player3"), 'error <Player3> does not exist')
        self.assertEqual(self.ui.command("Dealer1: Delete Player Player4"), 'error <Player4> does not exist')
        # invalid syntax entered
        self.assertEqual(self.ui.command("Dealer1: Delete (Player)_10"), 'Player username is invalid')
        self.assertEqual(self.ui.command("Dealer1: Delete (Player "), 'Player username is invalid')
        self.assertEqual(self.ui.command("Dealer1: Delete Player "), 'Player username is invalid')

    def test_invalidTableDelete(self):
        self.assertEqual(self.ui.command("Player1: start Table3 Player1,Player2"), "ERROR!")
        self.assertEqual(self.ui.command("Player2: start Table4 Player1,Player2"), "ERROR!")

    # The admin can view or reset the high score list
    #TODO

    #TODO
