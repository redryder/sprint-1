import unittest
from _AcceptanceTests.test_admins_to_table import TestAdmins
from _AcceptanceTests.test_admins_to_players import TestUsers
from _AcceptanceTests.test_player_bets import TestPlayerBets
from _AcceptanceTests.test_user_privleges import TestUserRights
from _AcceptanceTests.test_after_deal import TestPostDeal
from _AcceptanceTests.test_after_deal_bet import TestPostDealBet


# Right Click and run this file to run UnitTests.
# Sprint2 Additions: HandTest, PokerAdminTest, PlayerTest

def suite():
    # Add all the classes, each class will be a different "test Suite" just a group of tests to see if everything in the class is functioning correctly
    suites = [TestUsers, TestAdmins, TestUserRights, TestPlayerBets, TestPostDeal, TestPostDealBet]
    suite = unittest.TestSuite()
    for s in suites:
        suite.addTest(unittest.makeSuite(s))
        # this code will just loop through an array with all the class names and add them to the suite object.
    return suite


# code to run the tests; PyCharm does this automatically
# so this code could be removed in PyCharm
suite = suite()
runner = unittest.TextTestRunner()
res = runner.run(suite)
print(res)
print("*" * 20)
for i in res.failures:
    print(i[1])
