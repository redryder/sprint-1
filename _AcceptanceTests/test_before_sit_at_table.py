from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.dealer import Dealer
from PokerProject.classes.table import Table

class TestView(TestCase):
    def setUp(self):
        self.Dealer1 = Dealer('Dealer1', 'Password', True)

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')                            #creating players, dealer, and tables to use for tests
        self.Player4 = Player('Player4')
        self.Player5 = Player('Player5')
        self.Player6 = Player('Player6')
        self.Player7 = Player('Player7')

        self.Table1 = Table('Table1')
        self.Table2 = Table('Table2')
        self.Table3 = Table('Table3')

    ##############################################################################
    # Before sitting at a table, a player should be able to:
    # 1. View the status of the table
    # 2. View the high score list
    # 3. Sit at a table
    ##############################################################################
    def test_validSeeTables(self):          #test that the table and its status is visible to everyone
        self.ui.command("Login Dealer1 Password")
        self.ui.command("Dealer1: create Player Player1")
        self.ui.command("Dealer1: create Player Player2")
        self.ui.command("Dealer1: start Table1 Player1,Player2")

        self.ui.command("Login Player3")
        self.assertEqual(self.ui.command("Player3: show table status Table1"),
                         "Table: Table1\nNumber of Players:2\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer1\nIn Progress? True")

    def test_multiplePlayers(self):         #test that the table can hold multiple players
        self.ui.command("Login Dealer1 Password")
        self.ui.command("Dealer1: create Player Player1")       #create a player with a username
        self.ui.command("Dealer1: create Player Player2")
        self.ui.command("Dealer1: start Table1 Player1,Player2")

        self.ui.command("Login Player3")
        self.assertEqual(self.ui.command("Player3: show table status Table1"),
                         "Table: Table1\nNumber of Players:2\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer1\nIn Progress? True")

        self.ui.command("Dealer1: Table1 add Player3")
        self.ui.command("Login Player4")
        self.assertEqual(self.ui.command("Player4: show table status Table1"),
                         "Table: Table1\nNumber of Players:3\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer1\nIn Progress? True")

        self.ui.command("Login Player5")
        self.ui.command("Login Player6")
        self.ui.command("Dealer1: Table1 add Player4")
        self.ui.command("Dealer1: Table1 add Player5")
        self.ui.command("Dealer1: Table1 add Player6")

        self.ui.command("Login Player7")
        self.assertEqual(self.ui.command("Player7: show table status Table1"),
                         "Table: Table1\nNumber of Players:6\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer1\nIn Progress? True")

    def test_noTableSetUp(self):                            #the table can not be shown until it is set up
        self.ui.command("Login Player1")
        self.assertEqual(self.ui.command("Player1: show table status Table1"), "Error: Table1 has not been set up")

    def test_noExistingPlayer(self):                        #confirming that all players exist
        self.ui.command("Login Dealer1 Password")
        self.ui.command("Dealer1: create Player Player1")
        self.ui.command("Dealer1: create Player Player2")
        self.ui.command("Dealer1: start Table1 Player1,Player2")
        self.assertEqual(self.ui.command("Player3: show table status Table1"), "Error: Player3 does not exist")

    def test_multipleDealersAndTables(self):                #allow multiple players, tables, and dealers to be allowed on same server
        self.ui.command("Login Dealer1 Password")
        self.ui.command("Login Dealer2 Password")
        self.ui.command("Dealer1: create Player Player1")
        self.ui.command("Dealer1: create Player Player2")
        self.ui.command("Dealer1: start Table1 Player1,Player2")

        self.ui.command("Dealer2: create Player Player3")
        self.ui.command("Dealer2: create Player Player4")
        self.ui.command("Dealer2: start Table1 Player3,Player4")

        self.ui.command("Login Player5")
        self.assertEqual(self.ui.command("Player5: show table status Table1"),
                         "Table: Table1\nNumber of Players:2\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer1\nIn Progress? True")

        self.assertEqual(self.ui.command("Player5: show table status Table2"),
                         "Table: Table2\nNumber of Players:2\nMaximum Players: 6\nBuy in: 0\nDealer: Dealer2\nIn Progress? True")

    def test_playerViewScoresValid(self):
        pass

    def test_playerViewScoresInvalid(self):
        pass