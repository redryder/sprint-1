from django.test import TestCase

from PokerProject.models import Player, Table, Dealer


class TestStartWhenTwoPlayers(TestCase):

    def setUp(self):

        #Creates dealer1
        self.dealer1 = Dealer.objects.create(username="dealer1")

        #Creates 3 players
        self.player1 = Player.objects.create(username="player1")
        self.player2 = Player.objects.create(username="player2")
        self.player3 = Player.objects.create(username="player3")

        #Creates 3 empty tables
        self.table1 = Table.objects.create(name="table1")
        self.table2 = Table.objects.create(name="table2")
        self.table3 = Table.objects.create(name="table3")

        pass

    #Tests that when 2 players are at a table, the game is running
    def test_should_start_when_two_players(self):

        #Assume passing
        passing = True

        #If empty table game is running, fail
        if self.table1.currentGame:
            passing = False

        #Adds 2 players to the table
        self.ui.command("dealer1 table1 Add player1")
        self.ui.command("dealer1 table1 Add player2")

        #If the table is now running, pass, else, fail
        if self.table1.currentGame:
            pass
        else:
            passing = False

        #Show whether passed or failed
        self.assertTrue(passing)

        pass



    #Tests that a game should not be running when under two players at the table
    def test_should_not_start_when_under_two_players(self):

        #Assume it's passing
        passing = True

        #If at an empty table a game is running, fail
        if self.table2.currentGame:
            passing = False

        #Adds a player to the table
        self.ui.command("dealer1 table2 Add player1")

        #If a table with one player has the game running, fail
        if self.table2.currentGame:
            passing = False

        #Check whether passed or failed
        self.assertTrue(passing)

        pass


    #A game should be running if there's over two players at a table
    def test_should_start_when_over_two_players(self):

        #Assume it's passing
        passing = True

        #If at an empty table, the game is running, fail
        if self.table3.currentGame:
            passing = False

        #Adds 3 players to the table
        self.ui.command("dealer1 table3 Add player1")
        self.ui.command("dealer1 table3 Add player2")
        self.ui.command("dealer1 table3 Add player3")

        #If the game is running, pass, else, fail
        if self.table3.currentGame:
            pass
        else:
            passing = False

        #Check whether passed or failed
        self.assertTrue(passing)

        pass
