from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table
# from PokerProjectDjango.PokerProject.models import Command
from PokerProject.interface.ui import ui


class TestPlayerBets(TestCase):
    def setUp(self):

        self.ui = ui()

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')

        self.Table1 = Table('Table1')

    ##############################################################################
    # PBI#5 — As a player I should be able to see current chips held,
    # amount needed to call, and total pot, so I can decide to  raise, call, check, or fold on my turn (L)
    # Acceptance Criteria:
    # Each response should include the name of each player at the table, that players total chips,
    # that player's current bet, and status (call, raise, check, fold).
    # Maximum raise is 5 chips
    ##############################################################################
    # Player can check their chip count
    def test_invalidChipAmtCommand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.assertEqual(self.ui.command("Player1: chip amount"), "ERROR invalid command")
        self.assertEqual(self.ui.command("Player1: chip amounts"), "ERROR invalid command")
        self.assertEqual(self.ui.command("Player2: CHIP_AMOUNT"), "ERROR invalid command")

    def test_validChipAmtCommand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        # hand is randomly generated
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player2.show_hand() + " NUM_CARDS:2")
        self.ui.command("Player1: bet 5")
        # chips left
        self.assertEqual(self.ui.command("Player1: chip_amount"), "95")
        self.assertEqual(self.ui.command("Player2: chip_amount"), "100")

    # Player can check call amount
    def test_validChipCallAmtCommand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.assertEqual(self.ui.command("Player2: call_amount"), "5")
        self.assertEqual(self.ui.command("Player1: call_amount"), "0")

    # Player can check call amount
    def test_invalidChipCallAmtCommand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.assertEqual(self.ui.command("Player2: call amount"), "ERROR invalid command")
        self.assertEqual(self.ui.command("Player2: CALL_AMOUNT"), "ERROR invalid command")

    # Player can make calls >= chip value
    def test_validCall(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: bet 6")
        self.ui.command("Player1: bet 7")
        self.assertEqual(self.ui.command("Player2: call 7"), "Player2@Table1: CHIPS:87 STATUS:call BET:13 "
                                                             "HAND:" + self.Player1.show_hand() + " NUM_CARDS:4")

    def test_invalidCall(self):  # the call is over their chip amount
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: add 100 chips")
        self.assertEqual(self.ui.command("Player1: chip_amount"), "200")
        self.ui.command("Player1: bet 50")
        self.ui.command("Player2: bet 55")
        self.ui.command("Player1: bet 60")
        self.assertEqual(self.ui.command("Player2: call 60"), "ERROR call exceeds chip amount")

    # Player can make bets >= chip value

    # If bets >= call value
    def test_validBet(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: bet 6")
        self.ui.command("Player1: bet 7")
        self.assertEqual(self.ui.command("Player2: bet 7"), "Player2@Table1: CHIPS:87 STATUS:bet BET:13 "
                                                            "HAND:" + self.Player1.show_hand() + " NUM_CARDS:4")

    def test_invalidBet(self):  # (bet < call)
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: bet 6")
        self.ui.command("Player1: bet 7")
        self.assertEqual(self.ui.command("Player1: bet 0"), "ERROR INVALID BET AMOUNT")
        self.assertEqual(self.ui.command("Player1: bet -1000"), "ERROR INVALID BET AMOUNT")

    def test_invalidBet(self): # not enough chips
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: bet 6")
        self.assertEqual(self.ui.command("Player1: bet 1000"), "ERROR INVALID BET AMOUNT")
        self.assertEqual(self.ui.command("Player1: bet 96"), "ERROR INVALID BET AMOUNT")

    # Player can fold
    def test_validFoldCommand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Player2: fold"), "Player2 folds")
        self.assertEqual(self.ui.command("Player1: fold"), "ERROR game over")