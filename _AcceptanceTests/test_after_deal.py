from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table

class TestPostDeal(TestCase):
    def setUp(self):

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')            #creating players that can be tested
        self.Player3 = Player('Player3')

        self.Table1 = Table('Table1')               #create the test table

    def test_cards_shown(self):
        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: call 5")          #start and end round so that the cards get shown
        self.ui.command("Player3: fold")
        self.ui.command("Player1: call 5")


    def test_update_chips(self):        #Once the round is over, the winner is decided and the chips are updated.

        self.ui.command("Player1: bet 5")
        self.ui.command("Player2: call 5")
        self.ui.command("Player3: fold")
        self.ui.command("Player1: call 5")
        # this round is now over, test update chips for Player1 and Player2
        #TODO
        #not sure what to do here with testing what player won and how the chips get updated
