from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table
from PokerProject.interface.ui import ui


class TestUsers(TestCase):
    def setUp(self):

        self.ui = ui()

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')
        self.Player4 = Player('Player4')
        self.Player5 = Player('Player5')                                #creating players that can be tested
        self.Player6 = Player('Player6')
        self.Player7 = Player('Player7')
        self.Player8 = Player('Player8')
        self.Player9 = Player('Player9')
        self.Player10 = Player('Player10')
        self.Player11 = Player('Player11')

        self.Table1 = Table('Table1')
        self.Table2 = Table('Table2')
        self.Table3 = Table('Table3')





                                                                        # Dealer can create player accounts (username only, no passwords)
    #TODO



                                                                        # Created players must have 100 starting chips
    def test_validCreatedPlayerChipCount(self):

        self.assertEqual(self.ui.command("Player1 chip_amount"), 100)
        self.assertEqual(self.ui.command("Player2 chip_amount"), 100)
        self.assertEqual(self.ui.command("Player3 chip_amount"), 100)
        self.assertEqual(self.ui.command("Player4 chip_amount"), 100)
        self.assertEqual(self.ui.command("Player5 chip_amount"), 100)
        self.assertEqual(self.ui.command("Player6 chip_amount"), 100)

