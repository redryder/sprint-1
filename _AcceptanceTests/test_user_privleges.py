from django.test import TestCase
# from unittest import TestCase
from PokerProject.classes.player import Player
from PokerProject.classes.table import Table
# from PokerProjectDjango.PokerProject.models import Command
from PokerProject.interface.ui import ui


class TestUserRights(TestCase):
    def setUp(self):
        self.ui = ui()

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')

        self.Table1 = Table('Table1')

    ##############################################################################
    # PBI#2 — As a player, I want to use my username to issue commands, to identify my moves (S)
    # Player username is pre-pended to each command
    ##############################################################################
    # Commands with correct pre-pended usernames are accepted.
    def test_validUsernames(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2,Player3"), "Table1 in progress")
        self.assertEqual(self.ui.command("Player1: chip_amount"), "100")
        self.assertEqual(self.ui.command("Player2: chip_amount"), "100")
        self.assertEqual(self.ui.command("Player3: chip_amount"), "100")

    def test_validPlayerActions(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        # hand is randomly generated
        self.assertEqual(self.ui.command("Player1: chip_amount"), "100")
        self.assertEqual(self.ui.command("Player1: call_amount"), "0")
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player1: all in"), "Player1@Table1: CHIPS:0 STATUS:bet BET:100 "
                                                             "HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: fold"), "Player2 folds")

    # Commands with incorrect pre-pended usernames are not accepted.
    def test_invalidUsernames(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2,Player3"), "Table1 in progress")
        self.assertEqual(self.ui.command("Player_1: chip_amount"), "ERROR")
        self.assertEqual(self.ui.command("PLAYER2: chip_amount"), "ERROR")
        self.assertEqual(self.ui.command("PlayerThree: chip_amount"), "ERROR")

        # white space, case sensitive user name capitalized wrong,

    def test_invalidPlayerActions(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Player1: Create Player Player3"), 'ERROR')
        self.assertEqual(self.ui.command("Player2: Create Player Player3"), 'ERROR')
        self.assertEqual(self.ui.command("Player3: Create Player Player3"), 'ERROR')


    ##############################################################################
    # PBI#3 — As a Dealer, I want to use my username to issue commands, to identify my moves (S)
    # Dealer username is pre-pended to each command
    ##############################################################################
    # Commands with correct pre-pended usernames are accepted.
    def test_validDealerUsernames(self):
        self.assertEqual(self.ui.command("Login Dealer1 Password"),"Dealer1 Logged In")
        self.assertEqual(self.ui.command("Login Dealer2 Password"),"Dealer2 Logged In")
        self.assertEqual(self.ui.command("Login Dealer3 Password"),"Dealer3 Logged In")

    def test_validDealerActions(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: Create Player Player3"), 'the User <Player3> added')
        self.assertEqual(self.ui.command("Dealer1: Create Player Player4"), 'the User <Player4> added')
        self.assertEqual(self.ui.command("Dealer1: Create Player Player5"), 'the User <Player5> added')

    # Commands with incorrect pre-pended usernames are not accepted.
    def test_invalidDealerUsernames(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Login Dealer 1 Password"), "ERROR")
        self.assertEqual(self.ui.command("Login DEALER2 Password"), "ERROR")
        self.assertEqual(self.ui.command("Login DeAlER3 Password"), "ERROR")

    def test_invalidDealerActions(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        # hand is randomly generated
        self.assertEqual(self.ui.command("Dealer1: chip_amount"), "ERROR")
        self.assertEqual(self.ui.command("Dealer1: call_amount"), "ERROR")
        self.assertEqual(self.ui.command("Dealer1: show hand"), "ERROR")
        self.assertEqual(self.ui.command("Dealer1: all in"), "ERROR")
        self.assertEqual(self.ui.command("Dealer1: fold"), "ERROR")